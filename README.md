# Individual Project 2

## REST API

I used Rust to make a REST API of a todo list. The todo list has several functions:

1. Get entry: get all tasks in the current todo list.
2. Create entry: create a new task in the todo list.
3. Update entry: update the name of a existing task.
4. Delete entry: delete a task from the todo list.

## How to use

### Run the service

Make sure that your environment supports Rust. Then, use your IDE (vscode recommended) to run the project

```
cargo r -r
```

You can check the service is avaiable by visiting http://localhost:8080 to see if it works. If it works, it should look like this:

![Success image](API_success.png)

Then, you can try to use POSTMAN or Thunder Client to test the API. You can see how I did it by the demo video.

![alt text](createTask.png)

And here is the domo video if you wanna check:

<video width="320" height="240" controls>
  <source src="demo.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
