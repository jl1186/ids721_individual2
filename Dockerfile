# 使用官方的 Rust 镜像作为基础镜像
FROM rust:latest as builder

# 在容器中设置工作目录
WORKDIR /usr/src/app

# 复制依赖文件到工作目录
COPY . .

# 构建依赖
RUN cargo build --release

# 复制其余的应用源代码
COPY src ./src

# 构建你的应用
RUN cargo install --path .

# 开始一个新的阶段，并复制构建好的二进制文件
FROM debian:buster-slim
COPY --from=builder /usr/local/cargo/bin/todolist /usr/local/bin/todolist

# 暴露应用运行的端口
EXPOSE 8080

# 运行应用的命令
CMD ["todolist"]